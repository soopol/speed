/*
//////////////////////////////////////////////////////////////////
// ======================  FUNCTIONS ========================= //
////////////////////////////////////////////////////////////////
*/

function displayData(chart, value1, value2, duration){
	var data=[
				{ 
					"label": "One",
					"value" : value1
				} , 
				{ 
					"label": "Two",
					"value" : value2
				}];

    d3.select("#chart svg")
        .datum(data)
        .transition().duration(duration)
        .call(chart);
}
/*
//////////////////////////////////////////////////////////////////
// ======================  SOCKETIO ========================== //
////////////////////////////////////////////////////////////////
*/
$(document).ready(function(){
	var chart = nv.models.pieChart()
		.x(function(d) { return d.label })
		.y(function(d) { return d.value })
		.showLabels(false)     //Display pie labels
		.labelThreshold(0)  //Configure the minimum slice size for labels to show up
		.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
		.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
		.donutRatio(0.35)    //Configure how big you want the donut hole size to be.
		;

		var socket = io.connect(window.location.hostname);
		var room = $("#shareURL").data("room");

		$("#ready").click( function(){
	       	socket.emit("onReady", { timeout : 4000, first: true, round : 1 });
		});

		socket.on("connect",function(){


			//lobby
			if(window.location.pathname == "/lobby"){
				socket.emit("lobby");
			}

			//game
			else if(room){
				socket.emit("join",{room:room});
			}
			displayData(chart, 0,100,0);
		});

		socket.on("openGamesLobby",function(data){
			console.log("openGamesLobby");
			
			/*var content="<div class='row header'>\
				<div class='col-md-1'></div>\
				<div class='col-md-2'><span>Title</span></div>\
				<div class='col-md-5'><span>Description</span></div>\
				</div>";
			for(var i=0; i<data.open.length; i++){
			console.log(data.open[i]);
				content+="<div class='row open'>\
				<div class='col-md-1'><i style='color:#2ecc71;' class='fa fa-circle'></i></div>\
				<div class='col-md-2'><span>"+data.open[i].title+"</span></div>\
				<div class='col-md-5'><span>"+data.open[i].description+"</span></div>\
				<div class='col-md-4'><a href='http://"+window.location.host+"/"+data.open[i].url+"'>JOIN ROOM</a></div>\
				</div>";	
			}
			for(var i=0; i<data.full.length; i++){
			console.log(data.full[i]);
				content+="<div class='row full'>\
				<div class='col-md-1'><i style='color:#ff4d42;' class='fa fa-circle'></i></div>\
				<div class='col-md-2'><span>"+data.full[i].title+"</span></div>\
				<div class='col-md-5'><span>"+data.full[i].description+"</span></div>\
				<div class='col-md-4'><span class='full-button'>FULL</span></div>\
				</div>";	
			
			}
			$("#game-list").replaceWith("<div id='game-list'></div>");
			$("#game-list").append(content);*/


			var content="<thead style='color:#fff; text-transform:uppercase; letter-spacing:2px;'>\
				<th></th>\
				<th>Host</th>\
				<th>Description</th>\
				<th>&nbsp;</th>\
				</thead>";
			for(var i=0; i<data.open.length; i++){
			console.log(data.open[i]);
				content+="<tr>\
				<td style='width:10%'><i style='color:#2ecc71;' class='fa fa-circle'></td>\
				<td style='width:15%'><span style='color:#fff;'>"+data.open[i].title+"</span></td>\
				<td style='width:35%'><span style='color:#fff;'>"+data.open[i].description+"</span></td>\
				<td style='width:20%; text-align:center;'><a style='padding: 2px 0px; background-color:#333; color:#2ecc71; width:100px; display:inline-block;' href='http://"+window.location.host+"/"+data.open[i].url+"'>JOIN ROOM</a></tr>";	
			}
			for(var i=0; i<data.full.length; i++){
			console.log(data.full[i]);
				content+="<tr>\
				<td style='width:10%'><i style='color:#ff4d42;' class='fa fa-circle'></td>\
				<td style='width:15%'><span style='color:#7e8183;'>"+data.full[i].title+"</span></td>\
				<td style='width:35%'><span style='color:#7e8183;'>"+data.full[i].description+"</span></td>\
				<td style='width:20%; text-align:center;'><span style='padding: 2px 0px; background-color:#333; color:#7e8183; width:100px; display:inline-block; cursor:'>FULL</span></tr>";	
			}
			$("#game-list table").replaceWith("<table class='table'></table>");
			$("#game-list table").append(content);
		});


		socket.on("notify",function(data){
			/*
			DATA:
				data.success
				data.message
			*/
			if(data.success == 1){
				alertify.success(data.message);
			}
			else if(data.success == 0){
				alertify.log(data.message);
			}
			else{ //data.success == -1
				alertify.error(data.message);
			}
		});

		socket.on("showButton",function(){
			$("#ready").css("visibility", "visible");
		});

		socket.on("displayCountdown",function(data){
			// data.count

			var count=data.count;
			var stopinterval = setInterval(function(){
				$("#countdown").html(count);
				if(count==1){
					clearInterval(stopinterval);
				}
				count--;
			},1000);

		});
		socket.on("displayQuestion", function(data){
			$("#question").html(data.question);
			$("#ans0").html(data.answers[0]);
			$("#ans1").html(data.answers[1]);
			$("#ans2").html(data.answers[2]);
			$("#ans3").html(data.answers[3]);
			$("#ansKey").html(data.solution);
		});
		socket.on("displayRoundTime",function(data){
			$("#roundTime").html(data.time);
		});
		socket.on("displayTime",function(data){
		    displayData(chart, 100-data.time,data.time,data.transition);
		});

		socket.on("roundDone",function(data){
			/*
			DATA:
				data.round
				data.message
			*/

			if (data.round <= 5){
				console.log("in rounddone");
				socket.emit("reset", { room: room });
				socket.emit("onReady", {timeout : 0, first: false, round : data.round+1 });
			}
			else{
				console.log("game is done");
			}
		});

		$("#answer_input").keypress(function(e) {
	        if(e.which == 13) {
	        	console.log("pushed enter");
	            socket.emit("answered",{ room : room });
	            $(this).val("");
	        }
	    });

		socket.on("displayPoints", function(data){
			/*
			DATA:
				data.pid
				data.roundPoints
				data.totalPoints
			*/

			if(data.pid == 1){
				console.log("player 1 answered");
				$("#p1-score").html(data.totalPoints);
			}
			else{ //pid == 2
				console.log("player 2 answered");
				$("#p2-score").html(data.totalPoints);
			}
		});
});








