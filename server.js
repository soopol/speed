/*
//////////////////////////////////////////////////////////////////
// =======================  INITALIZE ======================== //
////////////////////////////////////////////////////////////////
*/

var express=require('express');
var async=require('async');
var app=express();
app.set('port', process.env.PORT || 3000);
var io = require("socket.io").listen(app.listen(app.get('port')) ,{log: false});

console.log("Listening on port http://localhost:" + app.get('port'));

app.use("/static", express.static(__dirname + "/static"));


/*
//////////////////////////////////////////////////////////////////
// =======================  FUNCTIONS ======================== //
////////////////////////////////////////////////////////////////
*/


function generateRoom(length) {
    var collection = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	var room="";

	for(var i=0; i<length; i++){
		room += collection.charAt(Math.floor(Math.random() * collection.length));
	}
	return room;
}

function generateTime(){
	return new Date().getTime()/1000;
}


function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function generateQuestion(round){
	var type = getRandomInt (0,3); 
	// 0 = add, 1 = subtract, 2 = multiply, 3 = division
	var signArr = ["+","-","*","/"];
	var difficulty = round + 7;
	var num1 = getRandomInt(2,difficulty);
	var num2 = getRandomInt(2,difficulty);
	var ans, pos1, pos2, pos3;
	if (type == 0){
		ans = num1 + num2;
		pos1 = ans + 1;
		pos2 = num1*10 + num2;
		pos3 = ans - 1;
	}
	else if (type == 1){
		ans = num1 - num2;
		if (ans != 0) pos1 = ans*(-1);
		else pos1 = num1*10 + num2;
		pos2 = ans - 1;
		pos3 = ans + 1;
	}
	else if (type == 2){
		ans = num1 * num2;
		pos1 = num1 * (num2-1);
		pos2 = (num1-1) * num2;
		pos3 = ans -1;
	}
	else if (type == 3){
		num1 = num1*num2;
		ans = num1/num2;
		pos1 = ans +1;
		pos2 = ans -1;
		pos3 = ans * 2;
	}
	var question = num1 + " " + signArr[type] + " " + num2;
	var answers = [ans, pos1, pos2, pos3];
	var key = getRandomInt (0,3);
	var temp = answers[key]; answers[key] = answers[0]; answers[0] = temp;
	var questionData = {"question" : question, "answers" : answers, "solution":key};
	return questionData;
}

function calculatePoints(elapsedTime){
	console.log(elapsedTime);
}
/*
//////////////////////////////////////////////////////////////////
// ==========================  URL =========================== //
////////////////////////////////////////////////////////////////
*/


app.get('/', function(req, res){
	res.render('index.jade');
});

app.get('/lobby', function(req, res){
	res.render('lobby.jade');
});

app.get('/room', function(req, res){
	room=generateRoom(6);
	res.render('main.jade', {shareURL: req.host+"/"+room, share: room});
});

app.get('/:room([a-zA-Z0-9]{6})', function(req, res){
	room=req.params.room;
	res.render('main.jade', {shareURL: req.host+"/"+room, share: room});
});


/*
//////////////////////////////////////////////////////////////////
// ======================  SOCKETIO ========================== //
////////////////////////////////////////////////////////////////
*/

 games={
		//contains all game instances
		/*{
			*room* : {
				player1: socket,
				player2: socket,
				title: string,
				description: string,
				time_created: time
			}
		}*/
		"aaaaaa":{
			title:"Insoo",
			description:"Lorem Ipsum is simply dummy",
		},
		"bbbbbb":{
			title:"Paul",
			description:"text of the printing and typesetting industry",
		},
		"cccccc":{
			title:"Jimmy",
			description:"Lorem Ipsum has been the industry's",
		},
		"dddddd":{
			title:"Taesung",
			description:"standard dummy text ever",
		},
		"eeeeee":{
			title:"Nithu",
			description:"since the 1500s, when an unknown",
		},
		"ffffff":{
			title:"Mikhail",
			description:"printer took a galley of type and scrambled",
		}
	}

openGames = [ 
		// contains all new & non-full rooms
		/* "room-instances" */ 
		"aaaaaa",
		"bbbbbb"
		]
fullGames = [ 
		// contains all new & non-full rooms
		/* "room-instances" */ 
		"cccccc",
		"dddddd",
		"eeeeee",
		"ffffff"
		]
/* SOCKET:
		//one player's socket instance
		{
			"room" : {a-zA-Z0-9}{6},
			"opponent" : socket,
			"points" : {0-9},
			"ready" : false,
			"done" : false
		}
*/
function generateLobbyData(data){
	var temp=[]

	for(var i=data.length-1; i>=0; i--){
		temp.push({
			"url":data[i],
			"title":games[data[i]].title,
			"description":games[data[i]].description
		});
	}
	return temp;
}

io.sockets.on("connection",function(socket){
	console.log("connection detected");
	// lobby
	socket.on("lobby",function(){
		socket.emit("openGamesLobby", { open:generateLobbyData(openGames), full:generateLobbyData(fullGames) });
	});


	// games

	socket.on("join",function(data){

		//initialize socket

		if(data.room in games){
			console.log("player 2 joined the game");
			//check if 2nd player already exists
			if(games[data.room].player2){
				socket.emit("full");
				return;
			}

			// initialize player 2
			socket.join(data.room);
			socket.set("room", data.room);
			socket.set("opponent", games[data.room].player1);
			games[data.room].player1.set("opponent",socket);
			socket.set("points",0);
			socket.set("ready", false);
			socket.set("answered", false);
			socket.set("correct", false);
			socket.set("pid", 2);

			games[data.room].player2=socket;

			openGames.splice(openGames.indexOf(data.room),1);
			console.log(openGames);
			fullGames.push(data.room);
			console.log(fullGames);
			socket.broadcast.emit("openGamesLobby", { open:generateLobbyData(openGames), full:generateLobbyData(fullGames) });

			io.sockets.in(data.room).emit("notify", { success:1, message:"Both players connected." } );

			io.sockets.in(data.room).emit("showButton");

		}
		// Player 1 has joined the room
		else{
			// initialize player 1
			console.log("player 1 joined the game");
			console.log(data.room);
			socket.join(data.room);
			socket.set("room", data.room);
			socket.set("points",0);
			socket.set("ready", false);
			socket.set("answered", false);
			socket.set("correct", false);
			socket.set("pid", 1);

			games[data.room] = {
				player1:socket,
				player2:null,
				title:"Guest",
				description:"Join me to play a game of SPEED !"
			};

			openGames.push(data.room);
			socket.broadcast.emit("openGamesLobby", { open:generateLobbyData(openGames), full:generateLobbyData(fullGames) });
		}
	});
	
	socket.on("onReady",function(data){
		async.parallel([
			socket.get.bind(this, "room"),
			socket.get.bind(this, "opponent"),
			socket.get.bind(this, "answered"),
			socket.get.bind(this, "points"),
			socket.get.bind(this, "pid")
	    ], function(err, results) {

	    	socket.set("ready", true);
	    	console.log("ready");

	    	results[1].get("ready", function(err,ready){
	    		// if both players are ready
	    		if(ready){
	    			console.log("both ready");
	    			// countdowns down in the frontend
	    			if (data.first){ io.sockets.in(results[0]).emit("displayCountdown", { count : 3 }); }

	    			// wait for countdown (5,4,3,2,1,start) == 6 seconds
					
					setTimeout(function(){
						console.log("START!!!!");
						//start time, generate question, emit it to front end
						
						var roundStartTime=generateTime();

						var elapsedTime, tempTime, roundTime = 0;
						var tempRoundCD = 5;

						console.log(roundStartTime);
						var timeCount = 1;
						var socketPoints = -1, opponentPoints = -1;
						// poll through the program
						var questionData = generateQuestion(data.round);
						io.sockets.in(results[0]).emit("displayQuestion", {question:questionData.question, answers:questionData.answers, solution:questionData.solution});
						console.log(questionData.question);
						console.log(questionData.answers);
						console.log(questionData.solution);
						var stopinterval=setInterval(function(){
							// if both finished, stop interval
							tempTime=generateTime();
							elapsedTime = tempTime-roundStartTime;
							
							roundCountDown=5-Math.floor(elapsedTime);

							//console.log(roundCountDown + " " + tempRoundCD);

							if( roundCountDown < tempRoundCD && roundCountDown > -1 ){
								tempRoundCD=roundCountDown;
								io.sockets.in(results[0]).emit("displayRoundTime", { time : tempRoundCD });
							}
							// 5 = round time limit, displayTime in %
							displayTime = ( (5-elapsedTime)/5 )* 100;
							
							//console.log(displayTime);
							
							//if(displayTime < 50){
							io.sockets.in(results[0]).emit("displayTime", { time : displayTime, transition: 150 });
							/*}
							else{
								io.sockets.in(results[0]).emit("displayTime", { time : displayTime });
							}*/
							// socket.on("check") will set 


							socket.get("answered",function(err,sAnswered){
								results[1].get("answered", function(err,answered){
									// if socket has answered
									if(sAnswered && socketPoints == -1){
										socketPoints= Math.round(10*(1+(displayTime/100)));//calculatePoints(elapsedTime);
										socket.set("points", results[3]+socketPoints);
										io.sockets.in(results[0]).emit("displayPoints", { pid : results[4], roundPoints : socketPoints, totalPoints : results[3]+socketPoints  });
									}
									// if has answered\
									if(answered && opponentPoints == -1){
										opponentPoints=Math.round(10*(1+(displayTime/100)));
										results[1].get("points", function(err, points){
											results[1].set("points", points+opponentPoints);
											results[1].get("pid", function(err, pid){
												io.sockets.in(results[0]).emit("displayPoints", { pid : pid, roundPoints : opponentPoints, totalPoints : points+opponentPoints  });
											});
										});
									}
									// if both answered or time runs out, start the next round 
									if((sAnswered && answered) || displayTime <= 0){
										console.log("end");
										if(displayTime<=0){
											io.sockets.in(results[0]).emit("displayTime", { time : 0, transition: 150 });
										}
										//elapsed time

										//socket.emit("displayPoints", { points : calculatePoints(finalTime) });
										
										//clear the interval
										clearInterval(stopinterval);
										console.log("round done");
										setTimeout(function(){
											io.sockets.in(results[0]).emit("displayTime", { time : 100, transition: 0});
											socket.emit("roundDone", { round : data.round });
											io.sockets.in(results[0]).emit("displayRoundTime", { time : 5 });
											socket.set("answered", false);
											results[1].set("answered", false);
										},1000);
									}
								});
							});
						},100);
					},data.timeout);
	    		}
	    		else{
	    			socket.emit("notify", { success:0, message:"Waiting on your friend to get ready..."});
	    			results[1].emit("notify", { success:0, message:"Your friend waiting on you!"});
	    		}
	    	});
	    });
	});
	socket.on("reset", function(data){
		console.log("reset");
		socket.set("ready", false);
	});

	socket.on("answered",function(data){
		
		if(data.room in games){
			console.log("answered");
			socket.set("answered", true);
		}
	});

	socket.on("disconnect",function(){
		socket.get("room",function(err,room){
	    	if(room in games){
				console.log("Disconnecting...");
				delete games[room];
				if(openGames.indexOf(room) > -1){
					openGames.splice(openGames.indexOf(room),1);
				}
				if(fullGames.indexOf(room) > -1){
					fullGames.splice(fullGames.indexOf(room),1);
				}
				console.log(openGames);
				console.log(fullGames);
				socket.broadcast.emit("openGamesLobby", { open:generateLobbyData(openGames), full:generateLobbyData(fullGames) });
			}
		});
	});
	/*socket.on("test",function(){
		socket.emit("countdown", { count : 5 });

		setTimeout(function(){
			console.log("hello");
		},6000);
	});*/
});








